# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121007043509) do

  create_table "access_rights", :force => true do |t|
    t.integer  "company_id"
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "active_admin_comments", :force => true do |t|
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "activities", :force => true do |t|
    t.integer  "company_id"
    t.integer  "person_id"
    t.text     "data"
    t.integer  "trigger_id"
    t.string   "type"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "activities", ["person_id"], :name => "index_activities_on_person_id"

  create_table "admin_users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true

  create_table "campaign_examples", :force => true do |t|
    t.string   "title"
    t.string   "owner"
    t.text     "description"
    t.text     "notes"
    t.text     "conditions"
    t.integer  "delivery_option"
    t.integer  "delay_val"
    t.string   "delay_unit"
    t.string   "delay_property"
    t.boolean  "enable_click_tracking"
    t.string   "subject"
    t.text     "text"
    t.string   "event"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.string   "slug"
    t.boolean  "active",                :default => false
    t.text     "extra_events"
    t.string   "image"
  end

  add_index "campaign_examples", ["slug"], :name => "index_campaign_examples_on_slug", :unique => true

  create_table "communication_occurrences", :force => true do |t|
    t.integer  "person_id"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.boolean  "development_mode",    :default => false
    t.integer  "variation_id"
    t.integer  "event_occurrence_id"
    t.string   "sent_to",             :default => ""
  end

  add_index "communication_occurrences", ["person_id"], :name => "index_email_logs_on_person_id"
  add_index "communication_occurrences", ["variation_id"], :name => "index_communication_occurrences_on_variation_id"

  create_table "communications", :force => true do |t|
    t.boolean  "active",                  :default => true
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.integer  "event_id"
    t.integer  "goal_event_id"
    t.text     "conditions"
    t.string   "scope",                   :default => ""
    t.boolean  "past_events"
    t.string   "title",                   :default => ""
    t.datetime "paused_at"
    t.boolean  "enable_click_tracking",   :default => true
    t.integer  "delay_val",               :default => 0
    t.integer  "delivery_option",         :default => 0
    t.string   "delay_property",          :default => ""
    t.integer  "conversions_count",       :default => 0
    t.integer  "preferred_delivery_time"
    t.string   "delay_unit",              :default => "days"
  end

  add_index "communications", ["event_id"], :name => "index_communications_on_event_id"
  add_index "communications", ["goal_event_id"], :name => "index_communications_on_goal_event_id"

  create_table "companies", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",                                          :null => false
    t.datetime "updated_at",                                          :null => false
    t.string   "api_key"
    t.string   "api_secret"
    t.text     "url"
    t.string   "auth_token"
    t.string   "preferred_code_language"
    t.string   "company_type"
    t.boolean  "view_development_mode",           :default => true
    t.boolean  "hide_health_bar"
    t.string   "referrer",                        :default => "site"
    t.boolean  "has_switched_to_live_view"
    t.integer  "events_count",                    :default => 0
    t.integer  "event_occurrences_count",         :default => 0
    t.integer  "communications_count",            :default => 0
    t.integer  "communication_occurrences_count", :default => 0
    t.string   "unsubscribe_url",                 :default => ""
    t.text     "event_data_keys"
    t.integer  "subscription_id"
    t.string   "unsubscribe_page"
    t.string   "stripeToken"
    t.boolean  "pay_yearly"
    t.string   "stripeCustomerId"
    t.boolean  "skip_connect_page"
    t.datetime "trial_ends_at"
    t.string   "coupon_code"
    t.boolean  "ga_converted",                    :default => false
    t.text     "people_data_keys"
    t.string   "domain"
    t.boolean  "custom_domain",                   :default => false
    t.string   "person_updated_url",              :default => ""
  end

  add_index "companies", ["auth_token"], :name => "index_companies_on_auth_token"

  create_table "contents", :force => true do |t|
    t.text     "text"
    t.integer  "template_occurrence_id"
    t.integer  "order"
    t.string   "tag"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  add_index "contents", ["template_occurrence_id"], :name => "index_contents_on_template_occurrence_id"

  create_table "conversions", :force => true do |t|
    t.integer  "communication_occurrence_id"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
    t.integer  "variation_id"
  end

  add_index "conversions", ["communication_occurrence_id"], :name => "index_conversions_on_communication_occurrence_id"
  add_index "conversions", ["variation_id"], :name => "index_conversions_on_variation_id"

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0
    t.integer  "attempts",   :default => 0
    t.text     "handler"
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "email_addresses", :force => true do |t|
    t.integer  "company_id"
    t.string   "from_name"
    t.string   "from_address"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "email_addresses", ["company_id"], :name => "index_email_addresses_on_company_id"

  create_table "email_providers", :force => true do |t|
    t.string   "name"
    t.text     "authentication"
    t.integer  "company_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "email_providers", ["company_id"], :name => "index_email_providers_on_company_id"

  create_table "event_data", :force => true do |t|
    t.integer  "event_occurrence_id"
    t.integer  "person_id"
    t.text     "key"
    t.text     "value"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
    t.integer  "event_id"
  end

  add_index "event_data", ["event_occurrence_id"], :name => "index_event_data_on_event_occurrence_id"
  add_index "event_data", ["key"], :name => "index_event_data_on_key"
  add_index "event_data", ["person_id"], :name => "index_event_data_on_person_id"

  create_table "event_occurrences", :force => true do |t|
    t.integer  "event_id"
    t.integer  "person_id"
    t.datetime "created_at",                          :null => false
    t.datetime "updated_at",                          :null => false
    t.boolean  "development_mode", :default => false
    t.text     "trigger_log"
  end

  add_index "event_occurrences", ["event_id"], :name => "index_logs_on_event_id"
  add_index "event_occurrences", ["person_id"], :name => "index_logs_on_person_id"

  create_table "events", :force => true do |t|
    t.string   "name"
    t.integer  "company_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.string   "api_event_name"
  end

  add_index "events", ["api_event_name"], :name => "index_events_on_api_event_name"
  add_index "events", ["company_id"], :name => "index_events_on_company_id"

  create_table "integrations", :force => true do |t|
    t.string   "name"
    t.text     "auth"
    t.integer  "company_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.text     "data"
    t.string   "type"
  end

  create_table "people", :force => true do |t|
    t.string   "email"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.integer  "company_id"
    t.datetime "last_event_occurrence"
    t.boolean  "development_mode",      :default => false
    t.boolean  "unsubscribed",          :default => false
  end

  add_index "people", ["company_id"], :name => "index_people_on_company_id"
  add_index "people", ["email"], :name => "index_people_on_email"

  create_table "people_data", :force => true do |t|
    t.integer  "person_id"
    t.string   "key"
    t.text     "value"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "people_data", ["key"], :name => "index_people_data_on_key"
  add_index "people_data", ["person_id"], :name => "index_people_data_on_person_id"

  create_table "ratings", :force => true do |t|
    t.integer  "value"
    t.string   "email"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "statistics", :force => true do |t|
    t.integer  "sent",         :default => 0
    t.integer  "delivered",    :default => 0
    t.integer  "opened",       :default => 0
    t.integer  "clicked",      :default => 0
    t.string   "campaign_id"
    t.integer  "variation_id"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
  end

  add_index "statistics", ["variation_id"], :name => "index_statistics_on_variation_id"

  create_table "taggings", :force => true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       :limit => 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id"], :name => "index_taggings_on_tag_id"
  add_index "taggings", ["taggable_id", "taggable_type", "context"], :name => "index_taggings_on_taggable_id_and_taggable_type_and_context"

  create_table "tags", :force => true do |t|
    t.string "name"
  end

  create_table "template_occurrences", :force => true do |t|
    t.string   "file_key"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.integer  "variation_id"
    t.integer  "template_id"
    t.boolean  "requires_update", :default => false
  end

  create_table "templates", :force => true do |t|
    t.string   "title"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "html"
    t.string   "file_key"
    t.integer  "company_id"
    t.text     "editable_tag_count"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "company_id"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "confirmation_token"
    t.string   "landing_page"
  end

  add_index "users", ["company_id"], :name => "index_users_on_company_id"
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "variations", :force => true do |t|
    t.string   "subject"
    t.integer  "communication_id"
    t.integer  "recipient_id"
    t.integer  "from_address_id"
    t.string   "bcc_address"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.text     "html_text",          :default => ""
    t.text     "plain_text",         :default => ""
    t.integer  "unsubscribes_count", :default => 0
  end

  add_index "variations", ["communication_id"], :name => "index_variations_on_communication_id"

end
