ActiveAdmin.register Company do
  menu :parent => "Models"
  
  index do
    column :id
    column :name
    column :url
    default_actions
  end

  form do |f|
    f.inputs "Company details" do
      f.input :name
      f.input :url, :as => :string
    end
    f.buttons
  end
end
