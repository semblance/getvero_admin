ActiveAdmin.register Person do
  menu :parent => "Models"

  form do |f|
    f.inputs "Person Details" do
      f.input :company
      f.input :email
      f.input :development_mode
    end
    f.buttons
  end
end