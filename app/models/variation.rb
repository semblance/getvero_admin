class Variation < ActiveRecord::Base
  belongs_to :communication
  has_many :communication_occurrences
  has_many :conversions
  has_one :template_occurrence

  attr_accessible :from_address_id, :recipient_id, :bcc_address, :campaign_id, :delay_val, :subject, :plain_text, :text
  attr_accessible :html_text, :only_business_days

  accepts_nested_attributes_for :template_occurrence
  attr_accessible :template_occurrence_attributes
end
