class StripeCustomer 
  Stripe.api_key = 'U2Ypu619U1GvXhnS0RioBNdMZSKEEdnT'

  def self.all
    @customers ||= Stripe::Customer.all(:count => 100)
    @customers[:data]
  end

  def self.all_subscribed
    result = []
    StripeCustomer.all.each do |cust|
      if cust.respond_to?(:subscription)
        result << cust
      end
    end
    return result
  end

  def self.find(id)
    Stripe::Customer.retrieve(id)
  end

  def self.avg_new_value_since(date)
    total = 0
    all_charges = StripeCharge.all
    cs = StripeCharge.new_since(date).each do |cust|
      cust_charges = []
      all_charges.each do |c|
        cust_charges << c if c.respond_to?(:customer) && c.customer == cust.id
      end
      total += cust_charges.first.amount if cust_charges.first
    end
    if cs.count == 0
      return '0'
    else
      return (total/cs.count)/100
    end
  end

  def self.avg_new_value_for_x_months_ago(x)
    total = 0
    cs = StripeCharge.new_for_x_months_ago(x).each do |c|
      total += c.amount
    end
    if cs.count == 0
      return '0'
    else
      return (total/cs.count)/100
    end
  end

end