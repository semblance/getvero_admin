class User < ActiveRecord::Base
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable, :registerable
  attr_accessible :email, :password, :password_confirmation, :remember_me, :company_id

  belongs_to :company
  validates :email, :uniqueness => true, :presence => true

  scope :from_this_month, where("created_at > ? AND created_at < ?", Time.now.beginning_of_month, Time.now.end_of_month)
  scope :from_x_months_ago, (lambda do |x| {:conditions => ["created_at > ? AND created_at < ?", (Time.now - x.months).beginning_of_month, (Time.now-x.months).end_of_month]} end)
end
