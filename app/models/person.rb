class Person < ActiveRecord::Base  
  belongs_to :company
  has_many :event_datas
  has_many :event_occurrences, :order => "created_at DESC"
  has_many :communication_occurrences
  has_many :people_datas

  attr_accessible :email, :development_mode, :unsubscribed, :company_id

  validates_presence_of :email
  validates_uniqueness_of :email, :scope => [:company_id, :development_mode]

  scope :development_scope, lambda { |t_or_f| where(:development_mode => t_or_f) }
end
