class EventData < ActiveRecord::Base
  belongs_to :event_occurrence
  belongs_to :person
  serialize :value
  
  attr_accessible :key, :value, :person_id

  validates_presence_of :key
end
