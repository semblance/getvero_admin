class CommunicationOccurrence < ActiveRecord::Base
  belongs_to :person
  belongs_to :variation
  belongs_to :event_occurrence

  attr_accessible :person_id, :variation_id, :event_occurrence_id, :development_mode

  scope :development_scope, lambda { |t_or_f| where(:development_mode => t_or_f) }
end
