class StripeCharge
  Stripe.api_key = 'U2Ypu619U1GvXhnS0RioBNdMZSKEEdnT'

  # All charges
  def self.all
    @charges ||= Stripe::Charge.all(:count => 100)
    @charges[:data]
  end

  def self.find(id)
    Stripe::Charge.retrieve(id)
  end

  # For specific customer
  def self.for_cust(id)
    Stripe::Charge.all(:customer => id)[:data]
  end

  # All charges since a certain date
  def self.total_since(date)
    total_charges = []
    StripeCharge.all.each do |c|
      if Time.at(c.created) > date
        total_charges << c
      end
    end
    return total_charges
  end

  # All charges for a certain month
  def self.total_for_x_months_ago(x)
    total_charges = []
    StripeCharge.all.each do |c|
      if Time.at(c.created) > (x.months.ago.beginning_of_month - 4.hours) && Time.at(c.created) < x.months.ago.end_of_month
        total_charges << c
      end
    end
    return total_charges
  end

  # New charges since a certain date
  def self.new_since(date)
    new_charges = []
    StripeCustomer.all.each do |c|
      if Time.at(c.created) > date
        new_charges << c
      end
    end
    return new_charges
  end

  # New charges for a certain month
  def self.new_for_x_months_ago(x)
    all_customers = StripeCustomer.all
    new_charges = []
    StripeCharge.all.each do |c|
      if Time.at(c.created) > x.months.ago.beginning_of_month && Time.at(c.created) < x.months.ago.end_of_month
        all_customers.each do |cust|
          if c.respond_to?(:customer) && cust.id == c.customer 
            is_new = if cust.respond_to?(:subscription) 
              Time.at(c.created) > Time.at(cust.subscription.start) - 2.minutes && Time.at(c.created) < Time.at(cust.subscription.start) + 2.minutes
            else 
              StripeCharge.for_cust(cust.id).last.created == c.created 
            end
            new_charges << c if is_new
          end
        end
      end
    end
    return new_charges
  end

  # Total revenue since a certain date
  def self.revenue_since(date)
    revenue = 0
    StripeCharge.total_since(date).each do |c|
      revenue += c.amount
    end
    return revenue/100
  end

  # Total revenue for a certain month
  def self.revenue_for_x_months_ago(x)
    revenue = 0
    StripeCharge.total_for_x_months_ago(x).each do |c|
      revenue += c.amount
    end
    return revenue/100
  end

  # Total revenue for this month
  def self.revenue_this_month
    customers = StripeCustomer.all_subscribed
    revenue = 0
    customers.each do |c|
      if c.respond_to?(:subscription)
        amt = c.subscription.plan.amount
        if c.respond_to?(:discount)
          revenue += amt * (1 - (c.discount.coupon.percent_off)/100.00)
        else
          revenue += amt
        end
      end
    end
    return revenue / 100.00
  end

end