class BlogVisitors
  extend Garb::Model

  metrics :uniquePageviews
  dimensions :pagePathLevel1 #, :nextPagePath
end
