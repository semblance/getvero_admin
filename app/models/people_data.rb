class PeopleData < ActiveRecord::Base
  belongs_to :person

  attr_accessible :key, :value

  validates_presence_of :key

  serialize :value
end
