class EmailProvider < ActiveRecord::Base
  attr_accessible :authentication, :company_id, :name
  belongs_to :company
  serialize :authentication

  validates :name, :authentication, :presence => true
end
