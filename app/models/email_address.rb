class EmailAddress < ActiveRecord::Base
  belongs_to :company
  attr_accessible :from_name, :from_address
end
