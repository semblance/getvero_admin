class TemplateOccurrence < ActiveRecord::Base
  belongs_to :variation
  
  attr_accessible :file_key, :variation_id
  attr_accessor :template_id
end