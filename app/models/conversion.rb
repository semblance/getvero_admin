class Conversion < ActiveRecord::Base
  belongs_to :variation
  belongs_to :communication_occurrence

  validates :variation_id, :communication_occurrence_id, :presence => true
end
