class Visitors
  extend Garb::Model

  metrics :visitors
  dimensions :month
end
