class Template < ActiveRecord::Base 
  belongs_to :company
  has_many :template_occurrences

  attr_accessible :title, :filename
end