class EventOccurrence < ActiveRecord::Base
  belongs_to :event
  belongs_to :person
  has_many :event_datas

  attr_accessible :event_id, :person_id, :development_mode

  scope :logs_since, lambda { |d| where(:created_at => [d..Time.now]) }
  scope :development_scope, lambda { |t_or_f| where(:development_mode => t_or_f) }

  validates_inclusion_of :development_mode, :in => [true, false]
end
