class GAnalytics
  def initialize
    Garb::Session.login('chris@getvero.com', 'webwarlock250')
    @profile = Garb::Management::Profile.all.detect {|p| p.web_property_id == 'UA-35338915-1'}
  end

  def visitors_by_month
    results = Visitors.results(@profile)
  end

  def visitors_for_month(m)
    result = 0
    visitors_by_month.each do |r|
      if r.month == m
        result = r.visitors
      end
    end
    return result.nil? ? 0 : result
  end

  def visitors_by_blog_post
    results = BlogVisitors.results(@profile, :filters => {:hostname.eql => 'blog.getvero.com', :pagePathLevel1.does_not_contain => 'category', :pagePathLevel1.does_not_contain => 'tag', :pagePathLevel1.does_not_contain => 'preview', :pagePathLevel1.does_not_contain => 'p=', :pagePathLevel1.does_not_contain => 'r44b', :pagePathLevel1.does_not_contain => 'fb', :pagePathLevel1.does_not_contain => 'shareadraft', :pagePathLevel1.does_not_match => '/'})
    return results.inject({}) {|h,v| h.merge!({v.page_path_level1 => v.unique_pageviews})}
  end
end