class Communication < ActiveRecord::Base
  belongs_to :event
  has_many :variations

  attr_accessible :title, :conditions, :enabled, :scope, :past_events, :event_id, :goal_event_id, :variations_attributes, :paused_at
  
  accepts_nested_attributes_for :variations
  validates_presence_of :variations

  serialize :conditions
end
