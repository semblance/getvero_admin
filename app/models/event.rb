class Event < ActiveRecord::Base
  belongs_to :company
  has_many :communications
  has_many :event_occurrences

  attr_accessible :name, :company_id, :api_event_name

  accepts_nested_attributes_for :communications, :allow_destroy => true
end