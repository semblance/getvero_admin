class Company < ActiveRecord::Base
  has_many :users
  has_many :people
  has_many :events
  has_many :event_occurrences, :through => :events
  has_many :communications, :through => :events
  has_many :variations, :through => :communications
  has_many :communication_occurrences, :through => :variations
  has_many :email_addresses, :dependent => :destroy
  has_many :templates
  has_one :email_provider

  attr_accessible :company_type, :preferred_code_language, :name, :api_key, :api_secret, :url, :unsubscribe_url, :subscription_id
  attr_accessible :sender_email, :step, :auth_token, :referrer, :events_attributes, :email_addresses_attributes, :event_data_keys
  attr_accessible :unsubscribe_page

  serialize :event_data_keys

  validates_presence_of :name, :api_key, :api_secret, :auth_token, :url, :sender_email, :step
  validates_numericality_of :step, :greater_than => 0, :less_than => 5, :only_integer => true
  
  accepts_nested_attributes_for :events, :allow_destroy => true
  accepts_nested_attributes_for :email_addresses, :allow_destroy => true

  # Months
  scope :from_x_months_ago, (lambda do |x| {:conditions => ["created_at > ? AND created_at < ?", (Time.now - x.months).beginning_of_month, (Time.now-x.months).end_of_month]} end)
  scope :subscribed_from_x_months_ago, (lambda do |x| {:conditions => ["subscription_id IS NOT NULL AND created_at > ? AND created_at < ?", (Time.now - x.months).beginning_of_month, (Time.now-x.months).end_of_month]} end)
  scope :free_trial_from_x_months_ago, (lambda do |x| {:conditions => ["subscription_id IS NULL AND trial_ends_at > ? AND created_at > ? AND created_at < ?", Time.now, (Time.now - x.months).beginning_of_month, (Time.now-x.months).end_of_month]} end)
  
  # Days
  scope :from_x_days_ago, (lambda do |x| {:conditions => ["created_at > ? AND created_at < ?", Time.now - x.days, Time.now]} end)
  scope :subscribed_from_x_months_ago, (lambda do |x| {:conditions => ["subscription_id IS NOT NULL AND created_at > ? AND created_at < ?", Time.now - x.days, Time.now]} end)
  scope :free_trial_from_x_months_ago, (lambda do |x| {:conditions => ["subscription_id IS NULL AND trial_ends_at > ? AND created_at > ? AND created_at < ?", Time.now, Time.now - x.days, Time.now]} end)


  def self.percent_tracked_event(i, days_or_months='days')
    count = 0 # Get a count of all companies with event_occurrences.count > 0 # Company.where :all, :include => :event_occurrences, :conditions => "children.id IS NULL"
    if days_or_months == 'months'
      total = Company.from_x_days_ago(i).count
      total == 0 ? 'Unknown' : count / total
    else
      total = Company.from_x_days_ago(i).count
      total == 0 ? 'Unknown' : count / total
    end
  end

  def self.percent_tracked_event_between(i, j, days_or_months='days')
    count = 0 # Get a count of all companies with event_occurrences.count > 0 # Company.where :all, :include => :event_occurrences, :conditions => "children.id IS NULL"
    if days_or_months == 'months'
      total = Company.from_x_months_ago(j).count
      total = total - Company.from_x_months_ago(i).count
      total == 0 ? 'Unknown' : count / total
    else
      total = Company.from_x_months_ago(j).count
      total = total - Company.from_x_days_ago(i).count
      total == 0 ? 'Unknown' : count / total
    end
  end

  def self.percent_sent_email(i, days_or_months='days')
    count = 0 # Get a count of all companies with communication_occurrences.count > 0 # Company.where :all, :include => :event_occurrences, :conditions => "children.id IS NULL"
    if days_or_months == 'months'
      total = Company.from_x_days_ago(i).count
      total == 0 ? 'Unknown' : count / total
    else
      total = Company.from_x_months_ago(i).count
      total == 0 ? 'Unknown' : count / total
    end
  end

  
end
