GetveroAdmin::Application.routes.draw do
  ActiveAdmin.routes(self)
  devise_for :admin_users, ActiveAdmin::Devise.config

  get '/dashboard' => "pages#dashboard"
  get '/choices' => "pages#choices"

  root :to => redirect('/choices')
end
